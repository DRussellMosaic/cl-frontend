import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import Container from '@material-ui/core/Container';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const htmlToText = require('html-to-text');
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
}));

function CourseList({availableCourses}) {
  const classes = useStyles();
  return (
    <GridList cellHeight={350} cols={4} >
      {availableCourses.courses.map((course) => {
        return <GridListTile key={course.courseimage} cols={1} style={{ height: 'auto' }}>
          <img src={course.courseimage} />
          <GridListTileBar
              title={course.fullname}
              subtitle={course.coursecategory}
              actionIcon={
                <IconButton aria-label={`info about ${course.fullname}`} className={classes.icon}>
                  <InfoIcon />
                </IconButton>
              }
            />
        </GridListTile>
      })}
    </GridList>
  );
}

export default CourseList;
