import React, {useEffect, useState} from 'react';
import './App.css';
import { AmplifyAuthenticator } from '@aws-amplify/ui-react';
import { Auth } from 'aws-amplify'
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from '@apollo/react-hooks';

import Home from './routes/Home'
import TitleBar from './components/TitleBar'

const client = new ApolloClient({
  // uri: 'http://localhost:5000/dev/hello'
  uri: 'https://smp1egwlxf.execute-api.us-east-1.amazonaws.com/dev/hello'
});

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    getUser();
    fetch('https://smp1egwlxf.execute-api.us-east-1.amazonaws.com/dev/hello',
    {
      method: 'POST',
      mode: 'cors'
    })
      .then(res => {
        console.log('test', res);
      })
      .catch(error => console.log(error))
  }, [])

  const getUser = () => {
   Auth.currentAuthenticatedUser()
     .then(user => setUser(user))
     .catch(error => console.log({ error }))
  };

  return (
    <div className="App">
      <AmplifyAuthenticator>
          <ApolloProvider client={client}>
            <TitleBar username={user && user.username}></TitleBar>
            <Home />
          </ApolloProvider>
      </AmplifyAuthenticator>
    </div>
  );
}

export default App;
