import React from 'react';
import Container from '@material-ui/core/Container';
import CourseList from '../components/CourseList';
import { gql } from "apollo-boost";
import { useQuery } from '@apollo/react-hooks';
import '../App.css';

const COURSES_AVAILABLE = gql`
  query {
    courses {
      fullname
      summary
      courseimage
      coursecategory
    }
  }
`;

function Home() {
   const { loading, error, data } = useQuery(COURSES_AVAILABLE);

   if (loading) return <p>Loading...</p>;
   if (error) {
     console.log(error);
     return <p>Error :(</p>;
   }

  return (
    <Container maxWidth="lg">
      <div id="availablecontainer">
        <h2 id="availableheader">Available Courses</h2>
      </div>
      <CourseList availableCourses={data} />
    </Container>
  );
}

export default Home;
